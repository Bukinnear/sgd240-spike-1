// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "BlankShooterGameMode.generated.h"

UCLASS(minimalapi)
class ABlankShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABlankShooterGameMode();
};



