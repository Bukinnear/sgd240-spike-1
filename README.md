---
Spike Report
---

Core 1 – Setting up your Development Environment
================================================

Introduction
------------

This spike report is used to create an outline and guide on the steps
required to create a git repository containing a blank Unreal Engine
project.

Goals
-----

1.  A git repository, which contains:

    a.  a proper gitignore file for Unreal Engine as the first commit

    b.  the Unreal Engine First Person Shooter C++ project, without
        Starter Materials

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K   
  Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

Provide resource information for team members who wish to learn
additional information from this spike.

-   Bitbucket: <https://bitbucket.org/>

-   Git: https://git-scm.com/downloads

-   Source Tree: <https://www.sourcetreeapp.com/>

-   Unreal .gitignore file:
    [https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore\#L1](https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore%23L1)

-   Git Workflow:
    <https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow>

-   Git LFS: https://www.atlassian.com/git/tutorials/git-lfs

Tasks undertaken
----------------

1.  Set up a new git repository

    a.  Create a new repository on the Bitbucket website. You will need
        access to a Bitbucket account

    b.  Clone the new repository using SourceTree (the link can be found
        on the top right of the repository’s page on the website)

2.  Create a new UE4 project

    a.  Open the UE4 editor

    b.  Choose New Project, select C++, First Person, no Starter Content

    c.  Save the Project to the newly created git repository

3.  Create & add a new .gitignore file

    a.  This can be accomplished in a number of ways:

        i.  You can download a complete file from the internet

        ii. You can create a new .gitignore using the link on the blank
            repository’s webpage

        iii. You can create a local file. To create a new .gitignore
            file on Windows, create a new .txt file & populate it as you
            see fit. Then navigate to the file in command line (CMD or
            PowerShell) and rename the file to ‘.gitignore’ (this cannot
            be done via the GUI, as windows does not allow blank
            filenames)

    b.  Add the file to the git repository

    c.  Ensure that it is properly set up to ignore the files from the
        project folder

4.  Upload to Bitbucket

    a.  Open the repository in SourceTree, and click Commit

    b.  Stage all of the files that need to be saved (or stage all)

    c.  Write a commit message to explain the changes

    d.  Click Commit (bottom right of the window)

    e.  Click Push to upload the commit remotely. Choose the branch to
        upload to in accordance to Git Workflow (to learn more about
        branches & Git Flow, see the next section).

5.  Git Large File Support

    a.  Git does not handle files over 10mb very well, so we use git LFS
        to alleviate this.

    b.  Open a git terminal in the local repository, either using CMD,
        Powershell, or the embedded Git install within SourceTree.

    c.  Type:

        i.  Git lfs install

        ii. (and for UE4 projects):

        iii. Git lfs track \*.umap

        iv. Git lfs track \*.uasset

What we found out
-----------------

##### Git Workflow:

Git workflow, hence the name, is a style of workflow that helps organize
and maintain a project managed by git. It is a set of guidelines that
does not need to be strictly adhered to, but is often beneficial to do
so.

The core principle of git workflow lies in the use of branches:

1.  The master branch is used for stable code only, and is usually
    tagged with a version number.

2.  A secondary ‘development’ branch is used for testing and debugging
    new code and features.

3.  Further ‘feature’ branches are employed to separately create and
    implement new features.

Once a feature has been developed, it is merged into the development
branch, which is then used for further testing and evaluation. Once code
has been deemed stabled and ready to be released, it is merged into the
master branch and tagged.

For larger projects, the use of ‘release candidate’ and ‘hotfix’
branched may also be employed, amongst a variety of other things, and
although those are outside the scope of this report, the principle
remains the same. Each branch has it’s own purpose, and once it’s task
is complete, it is merged into the next level down.

##### Other Lessons Learnt:

-   We learnt how to create a new Unreal Engine First Person Shooter C++
    project, without Starter Materials

-   We learnt how to create a new git repository using Bitbucket, and
    upload our project files using SourceTree.

-   Windows does not accept blank filenames, requiring a workaround to
    create a new .gitignore file

-   Git by default does not handle files over 10mb very well, and to
    account for this we use git LFS.

-   We learnt about Git Workflow.

Open Issues/risks
-----------------

1.  It is assumed that the user has access to a Bitbucket account, Git,
    and SourceTree. This process *can* be completed without Bitbucket
    and/or SourceTree, however that goes beyond the scope of this
    report.

2.  We did not learn anything about C++ in this spike, as it will be
    covered in a later report.
